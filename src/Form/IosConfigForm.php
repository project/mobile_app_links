<?php

namespace Drupal\mobile_app_links\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\file\Entity\File;

/**
 * Class IosConfigForm.
 */
class IosConfigForm extends ConfigFormBase {

  const CONFIG_NAME = 'mobile_app_links.ios';

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'mobile_app_links_ios_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function getEditableConfigNames(): array|string {
    return [self::CONFIG_NAME];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildForm($form, $form_state);

    $config = $this->config(self::CONFIG_NAME);
    $applinks = (array) $config->get('applinks');

    $form['#tree'] = TRUE;
    $form['applinks'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('iOS App Configurations'),
      '#prefix' => '<div id="configurations-fieldset-wrapper">',
      '#suffix' => '</div>',
    ];
    $temp_count = (count($applinks) == 0) ? ($form_state->get('temp_count') + 1) : $form_state->get('temp_count');
    if (!empty($applinks)) {
      foreach ($applinks as $key => $applink) {
        $form['applinks'][$key] = [
          '#type' => 'fieldset',
          '#Collapsible' => TRUE,
        ];

        $form['applinks'][$key]['appID'] = [
          '#type' => 'textfield',
          '#title' => $this->t('App ID'),
          '#description' => $this->t('Enter one value per line (including team ID).'),
          '#default_value' => $applink['appID'],
        ];

        $form['applinks'][$key]['paths'] = [
          '#type' => 'textarea',
          '#title' => $this->t('Paths'),
          '#description' => $this->t('Enter one value per line.'),
          '#default_value' => $applink['paths'],
        ];

        $form['applinks'][$key]['appclips'] = [
          '#type' => 'textfield',
          '#title' => $this->t('App Clips'),
          '#description' => $this->t('Enter the "apps" that have appclips: *your_id*.com.domain.Clip.'),
          '#default_value' => $applink['appclips'],
        ];

        $form['applinks'][$key]['appids'] = [
          '#type' => 'multivalue',
          '#title' => $this->t('App IDs'),
          'appids' => [
            '#type' => 'textfield',
          ],
          '#default_value' => $applink['appIDs'],
          '#description' => $this->t('Enter the APP IDs'),
          '#description_display' => 'before',
        ];
        $form['applinks'][$key]['defaults'] = [
          '#type' => 'textarea',
          '#title' => $this->t('Defaults'),
          '#description' => $this->t('Enter one value per line as "caseSensitive : false".'),
          '#default_value' => $applink['defaults'],
        ];
        $form['applinks'][$key]['components'] = [
          '#type' => 'managed_file',
          '#title' => $this->t('Upload File'),
          '#upload_location' => 'public://uploads/',
          '#upload_validators' => [
            'FileExtension' => ['extensions' => 'json'],
          ],
          '#default_value' => isset($applink['components']) ? [$applink['components']] : '',
          '#description' => t('Please upload components data with json file.'),
        ];

        $form['applinks'][$key]['applink_delete'] = [
          '#type' => 'submit',
          '#value' => $this->t('Delete'),
          '#submit' => ['::deleteThis'],
          '#name' => 'remove-button' . $key,
          '#ajax' => [
            'callback' => '::addRemoveCallback',
            'wrapper' => 'configurations-fieldset-wrapper',
          ],
        ];
      }
    }

    if ($temp_count > 0) {
      for ($i = 0; $i < $temp_count; $i++) {
        $form['applinks'][$i] = [
          '#type' => 'fieldset',
          '#Collapsible' => TRUE,
        ];
        $form['applinks'][$i]['appID'] = [
          '#type' => 'textfield',
          '#title' => $this->t('App ID'),
          '#description' => $this->t('Enter one value per line (including team ID).'),
          '#attributes' => [
            'id' => 'relation_type_new_' . $i,
          ],
          '#default_value' => '',
        ];

        $form['applinks'][$i]['paths'] = [
          '#type' => 'textarea',
          '#title' => $this->t('Paths'),
          '#description' => $this->t('Enter one value per line.'),
          '#default_value' => '',
        ];

        $form['applinks'][$i]['appclips'] = [
          '#type' => 'textfield',
          '#title' => $this->t('App Clips'),
          '#description' => $this->t('Enter the "apps" that have appclips: *your_id*.com.domain.Clip.'),
          '#default_value' => '',
        ];
        $form['applinks'][$i]['appids'] = [
          '#type' => 'multivalue',
          '#title' => $this->t('App IDs'),
          'appids' => [
            '#type' => 'textfield',
          ],
          '#description' => $this->t('Enter the APP IDs'),
          '#description_display' => 'before',
        ];
        $form['applinks'][$i]['defaults'] = [
          '#type' => 'textarea',
          '#title' => $this->t('Defaults'),
          '#description' => $this->t('Enter one value per line as "caseSensitive : false".'),
          '#default_value' => '',
        ];
        $form['applinks'][$i]['components'] = [
          '#type' => 'managed_file',
          '#title' => $this->t('Upload File'),
          '#upload_location' => 'public://uploads/',
          '#upload_validators' => [
            'FileExtension' => ['extensions' => 'json'],
          ],
          '#description' => t('Please upload components data with json file.'),
        ];
      }
    }

    $form['actions'] = [
      '#type' => 'actions',
    ];


    $form['applinks']['actions']['applinks_add'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add More'),
      '#submit' => ['::addOne'],
      '#ajax' => [
        'callback' => '::addRemoveCallback',
        'wrapper' => 'configurations-fieldset-wrapper',
      ],
    ];

    $form['webcredentials'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Shared Web Credentials'),
      '#description' => $this->t('Enter one app ID per line (including team ID) that should share credentials with this site.'),
      '#default_value' => $config->get('webcredentials'),
    ];

    $form_state->setCached(FALSE);

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function addOne(array &$form, FormStateInterface $form_state) {
    $applinks_field = $form_state->get('temp_count') ?? 0;
    $form_state->set('temp_count', ($applinks_field + 1));

    $form_state->setRebuild();
  }

  /**
   * {@inheritdoc}
   */
  public function deleteThis(array &$form, FormStateInterface $form_state) {
    $config = $this->config(self::CONFIG_NAME);

    $triggering_element = $form_state->getTriggeringElement();
    $key = $triggering_element['#array_parents'][1];

    // Delete config.
    $config->clear('applinks.' . $key);
    $config->save();

    $form_state->setRebuild();
  }

  /**
   * {@inheritdoc}
   */
  public function addRemoveCallback(array &$form, FormStateInterface $form_state) {
    return $form['applinks'];
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config(self::CONFIG_NAME);
    $applinks = [];
    $values = $form_state->getValue('applinks');
    $index = 0;
    foreach ($values as $value) {
      if (!empty($value['appID'])) {
        $config_key = str_replace('.', '-', $value['appID']) . '_' . $index;
        $applinks[$config_key]['applinks'] = $value['applinks'] ?? '';
        // Format appids.
        $app_ids = $value['appids'];
        $formatted_appids = [];
        foreach ($app_ids as $key => $app_value) {
          if (!empty($app_value['appids'])) {
            $formatted_appids[$key] =  $app_value['appids'];
          }
          else {
            unset($app_ids[$key]);
          }
        }
        $applinks[$config_key]['appID'] = $value['appID'];
        $applinks[$config_key]['paths'] = str_replace("\r", PHP_EOL, str_replace("\r\n", PHP_EOL, $value['paths']));
        $applinks[$config_key]['appclips'] = $value['appclips'];
        $applinks[$config_key]['appIDs'] = $formatted_appids;
        $applinks[$config_key]['defaults'] = str_replace("\r", PHP_EOL, str_replace("\r\n", PHP_EOL, $value['defaults']));
        if (!empty($value['components'][0])) {
          $file = File::load($value['components'][0]);
          $file->setPermanent();
          $file->save();
          $applinks[$config_key]['components'] = $value['components'][0];
        }
      }
      $index++;
    }
    $config->set('applinks', $applinks);
    $config->set('webcredentials', trim(preg_replace('~\r\n?~', "\n", $form_state->getValue('webcredentials'))));
    $config->save();

    return parent::submitForm($form, $form_state);
  }

}
