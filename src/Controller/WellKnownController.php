<?php

namespace Drupal\mobile_app_links\Controller;

use Drupal\Core\Cache\CacheableJsonResponse;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Cache\CacheableResponse;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Http\Exception\CacheableNotFoundHttpException;
use Drupal\mobile_app_links\Form\AndroidConfigForm;
use Drupal\mobile_app_links\Form\AppleDevIdAssocConfigForm;
use Drupal\mobile_app_links\Form\AppleDevMerchantIdAssocConfigForm;
use Drupal\mobile_app_links\Form\IosConfigForm;
use Drupal\file\Entity\File;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Customer controller to .well-known links.
 */
class WellKnownController extends ControllerBase {

  /**
   * Current request.
   *
   * @var \Drupal\Core\Http\RequestStack
   */
  protected $request;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->request = $container->get('request_stack');
    return $instance;
  }

  /**
   * Page callback for .well-known/assetlinks.json.
   *
   * @return \Drupal\Core\Cache\CacheableJsonResponse
   *   JSON Response.
   */
  public function assetLinks(): CacheableJsonResponse {
    $config = $this->config(AndroidConfigForm::CONFIG_NAME);

    // Handle caching.
    $cacheMeta = new CacheableMetadata();
    $cacheMeta->addCacheTags($config->getCacheTags());

    $body = [];
    $android_packages = $config->get('android_packages');
    if (!empty($android_packages)) {
      foreach ($android_packages as $android_package) {
        $item = [
          'relation' => [
            $android_package['relation'] ?? 'delegate_permission/common.handle_all_urls',
          ],
          'target' => [
            'namespace' => 'android_app',
            'package_name' => $android_package['package_name'],
            'sha256_cert_fingerprints' => explode(PHP_EOL, $android_package['sha256_cert_fingerprints']),
          ],
        ];

        $body[] = $item;
      }
    }

    $sites = [];
    if ($config->get('web_statements.include_current_site')) {
      $sites[] = $this->request->getCurrentRequest()->getSchemeAndHttpHost();
    }

    $additional_sites = $config->get('web_statements.additional_sites');
    if (!empty($additional_sites)) {
      $sites = array_merge($sites, explode(PHP_EOL, $additional_sites));
    }

    foreach ($sites as $site) {
      $site = trim($site);
      if (empty($site)) {
        continue;
      }

      $body[] = [
        'relation' => [
          'delegate_permission/common.get_login_creds',
        ],
        'target' => [
          'namespace' => 'web',
          'site' => $site,
        ],
      ];
    }

    $response = new CacheableJsonResponse(json_encode($body, JSON_UNESCAPED_SLASHES), 200, [], TRUE);
    $response->addCacheableDependency($cacheMeta);
    return $response;
  }

  /**
   * Page callback for apple-developer-merchantid-domain-association.txt file.
   *
   * @return \Drupal\Core\Cache\CacheableResponse
   *   Response.
   * @throws \Drupal\Core\Http\Exception\CacheableNotFoundHttpException
   */
  public function getAppleDevId(): CacheableResponse {
    $config = $this->config(AppleDevIdAssocConfigForm::CONFIG_NAME);

    // Handle caching.
    $cacheMeta = new CacheableMetadata();
    $cacheMeta->addCacheTags($config->getCacheTags());

    $body = $config->get('apple_dev_id_assoc');

    if (empty($body)) {
      throw new CacheableNotFoundHttpException($cacheMeta);
    }

    $response = new CacheableResponse($body, 200, ['Content-Type' => 'text/plain']);
    $response->addCacheableDependency($cacheMeta);
    return $response;
  }

  /**
   * Page callback for apple-developer-merchantid-domain-association.txt file.
   *
   * @return \Drupal\Core\Cache\CacheableResponse
   *   Response.
   */
  public function getAppleDevMerchantId(): CacheableResponse {
    $config = $this->config(AppleDevMerchantIdAssocConfigForm::CONFIG_NAME);

    // Handle caching.
    $cacheMeta = new CacheableMetadata();
    $cacheMeta->addCacheTags($config->getCacheTags());

    $body = $config->get('apple_dev_merchant_id_assoc');

    if (empty($body)) {
    }

    $response = new CacheableResponse($body, 200, ['Content-Type' => 'text/plain']);
    $response->addCacheableDependency($cacheMeta);
    return $response;
  }

  /**
   * Page callback for .well-known/apple-app-site-association.
   *
   * @return \Drupal\Core\Cache\CacheableJsonResponse
   *   JSON Response.
   */
  public function appleAppSiteAssociation(): CacheableJsonResponse {
    $config = $this->config(IosConfigForm::CONFIG_NAME);

    // Handle caching.
    $cacheMeta = new CacheableMetadata();
    $cacheMeta->addCacheTags($config->getCacheTags());
    $applinks = $config->get('applinks');
    $body = [];

    if (!empty($applinks)) {
      $body['applinks'] = [
        'apps' => [],
      ];
      $details = [];
      foreach ($applinks as $applink) {
        $components_json = '';
        $appID = $applink['appID'];
        if (!empty($appID)) {
          $components = $applink['components'] ?? '';
          if (!empty($components)) {
            $file = File::load($components);
            if ($file) {
              $components_json = json_decode(file_get_contents($file->getFileUri()), TRUE);
            }
          }
          $def_val_arr = [];
          if (!empty($applink['defaults'])) {
            foreach (explode(PHP_EOL, $applink['defaults']) as $def_val) {
              list($key, $val) = explode(':', $def_val);
              $def_val_arr[trim($key)] = (boolean) trim($val);
            }
          }
          $details[] = array_filter([
            'appID' => $appID,
            'paths' => $applink['paths'] ? explode(PHP_EOL, $applink['paths']) : '',
            'appIDs' => $applink['appIDs'],
            'components' => $components_json ?? '',
            'defaults' => $def_val_arr ?? '',
          ]);
        }
      }
      $body['applinks']['details'] = $details;
    }

    $webcredentials = $config->get('webcredentials');
    if (!empty($webcredentials)) {
      $body['webcredentials']['apps'] = explode(PHP_EOL, $webcredentials);
    }

    $appClips = $config->get('appclips');
    if (!empty($appClips)) {
      $body['appclips'] = [
        'apps' => [
          $appClips,
        ],
      ];
    }

    if (empty($body)) {
      throw new CacheableNotFoundHttpException($cacheMeta);
    }

    $response = new CacheableJsonResponse(json_encode($body, JSON_UNESCAPED_SLASHES), 200, [], TRUE);
    $response->addCacheableDependency($cacheMeta);
    return $response;
  }

}
